在ssh -T git@gitee.com 时，连接被拒绝，后来使用如下命令调试出现：
```
ssh -vT git@gitee.com
```
错误信息：
sign_and_send_pubkey: signing failed: agent refused operation

解决方案：
Looks like an ssh-agent is running already but it can not find any keys attached. To solve this add the private key identities to the authentication agent like so:

```
ssh-add
```
Then you can ssh into your server.

in addition, you can see the list of fingerprints of all identities currently added by:
```
ssh-add -l
```
