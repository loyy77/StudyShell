#bash select_command.sh
## output:
## 1) a
## 2) b
## 3) c
## #?

fun1(){
select item in a b c;
do
	echo "your choice ${item} ($REPLY)"
	break	
done
}

## input:
## bash select_command.sh a b c
## output:
## 1) a
## 2) b
## 3) c
## #?
fun2(){
	select item;
	do
		echo "your choice ${item} ($REPLY)"
	done
}
